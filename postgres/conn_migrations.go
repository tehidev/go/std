package postgres

import (
	"context"
	"embed"
	"fmt"
	"io/fs"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type migrationFile struct {
	Num  int
	Name string
	Text string
	IsUp bool
}

func (conn *Conn) Migrate(ctx context.Context) error {
	return conn.MigrateTo(ctx, 99999999)
}

func (conn *Conn) MigrateTo(ctx context.Context, targetNumber int) error {
	_, log := conn.logger.Branch(ctx, "postgres")
	defer log.End()

	log.Debug().Msg("run migrations")

	if err := conn.migrationCheckTable(ctx); err != nil {
		return err
	}

	files, err := getMigrationsFiles(conn.migrations)
	if err != nil {
		return err
	}

	if len(files) == 0 {
		log.Debug().Msg("no migrations files")
		return nil
	}

	upFiles := map[int]*migrationFile{}
	downFiles := map[int]*migrationFile{}

	maxNum := 0
	for _, file := range files {
		if file.Num > maxNum {
			maxNum = file.Num
		}
		if file.IsUp {
			upFiles[file.Num] = file
		} else {
			downFiles[file.Num] = file
		}
	}
	if targetNumber > maxNum {
		targetNumber = maxNum
	}

	wasMigrations := false

	for num := 1; num <= targetNumber; num++ {
		migration, ok := upFiles[num]
		if !ok {
			continue
		}
		exist, err := conn.migrationExist(ctx, num)
		if err != nil {
			return err
		}
		if exist {
			continue
		}
		log.Debug().Msgf("+ %s", migration.Name)

		if migration.Text != "" {
			err = conn.Tx(ctx, func(tx Tx) error {
				_, err := tx.Exec(ctx, migration.Text)
				return err
			})
			if err != nil {
				return err
			}
		}

		if err := conn.migrationWrite(ctx, migration.Num, migration.Name); err != nil {
			return err
		}

		wasMigrations = true
	}

	for num := maxNum; num > targetNumber; num-- {
		migration, ok := downFiles[num]
		if !ok {
			continue
		}
		exist, err := conn.migrationExist(ctx, num)
		if err != nil {
			return err
		}
		if !exist {
			continue
		}
		log.Debug().Msgf("- %s", migration.Name)

		if migration.Text != "" {
			err = conn.Tx(ctx, func(tx Tx) error {
				_, err := tx.Exec(ctx, migration.Text)
				return err
			})
			if err != nil {
				return err
			}
		}

		if err := conn.migrationDelete(ctx, migration.Num); err != nil {
			return err
		}

		wasMigrations = true
	}

	if wasMigrations {
		log.Debug().Msg("migrations done")
	} else {
		log.Debug().Msg("no migrations for apply")
	}
	return nil
}

func (conn *Conn) migrationCheckTable(ctx context.Context) error {
	q := `CREATE TABLE IF NOT EXISTS %s (
		id      SERIAL    PRIMARY KEY,
		created TIMESTAMP WITH TIME ZONE,
		number  INT,
		name    TEXT
	)`
	q = fmt.Sprintf(q, conn.migrationsTable)
	_, err := conn.Exec(ctx, q)
	return err
}

func (conn *Conn) migrationExist(ctx context.Context, num int) (exist bool, err error) {
	q := `SELECT EXISTS (SELECT id FROM %s WHERE number = $1)`
	q = fmt.Sprintf(q, conn.migrationsTable)
	err = conn.QueryRow(ctx, q, num).Scan(&exist)
	return
}

func (conn *Conn) migrationDelete(ctx context.Context, num int) error {
	q := `DELETE FROM %s WHERE number = $1`
	q = fmt.Sprintf(q, conn.migrationsTable)
	_, err := conn.Exec(ctx, q, num)
	return err
}

func (conn *Conn) migrationWrite(ctx context.Context, num int, name string) error {
	q := `INSERT INTO %s (created, number, name) VALUES ($1,$2,$3)`
	q = fmt.Sprintf(q, conn.migrationsTable)
	_, err := conn.Exec(ctx, q, time.Now(), num, name)
	return err
}

func getMigrationsFiles(migrations *embed.FS) (files []*migrationFile, err error) {
	err = fs.WalkDir(migrations, ".", func(path string, d fs.DirEntry, err error) error {
		if err != nil || d.IsDir() || filepath.Ext(path) != ".sql" {
			return nil
		}
		name := filepath.Base(path)
		ni := strings.IndexByte(name, '_')
		num, err := strconv.Atoi(name[:ni])
		if err != nil {
			return err
		}
		data, err := migrations.ReadFile(path)
		if err != nil {
			return err
		}
		files = append(files, &migrationFile{
			Num:  num,
			Name: name,
			Text: string(data),
			IsUp: strings.Contains(path, ".up."),
		})
		return err
	})
	for i := 0; i < len(files); i++ {
		for j := i + 1; j < len(files); j++ {
			if files[i].Name > files[j].Name {
				files[i], files[j] = files[j], files[i]
			}
		}
	}
	return
}
