package postgres

import (
	"context"
	"embed"
	"fmt"
	"os"
	"strings"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"

	"gitlab.com/tehidev/go/monolog"
)

func New() *ConnectionBuilder {
	return &ConnectionBuilder{
		conn: &Conn{
			migrationsTable: "app_migrations",
			logger: monolog.New().
				WithWriter(os.Stdout).
				Build(),
		},
		address:  "127.0.0.1:5432",
		database: "postgres",
		username: "postgres",
		password: "",
	}
}

type ConnectionBuilder struct {
	conn *Conn

	address  string
	database string

	username string
	password string
}

func (b *ConnectionBuilder) WithLogger(logger monolog.Logger) *ConnectionBuilder {
	b.conn.logger = logger
	return b
}

func (b *ConnectionBuilder) WithAddress(address string) *ConnectionBuilder {
	if !strings.ContainsRune(address, ':') {
		address += ":5432"
	}
	b.address = address
	return b
}

func (b *ConnectionBuilder) WithDatabase(dbName string) *ConnectionBuilder {
	b.database = dbName
	return b
}

func (b *ConnectionBuilder) WithCredentials(username, password string) *ConnectionBuilder {
	b.username = username
	b.password = password
	return b
}

func (b *ConnectionBuilder) WithMigrationsTable(tableName string) *ConnectionBuilder {
	b.conn.migrationsTable = tableName
	return b
}

func (b *ConnectionBuilder) WithEmbeddedMigrations(migrations *embed.FS) *ConnectionBuilder {
	b.conn.migrations = migrations
	return b
}

func (b *ConnectionBuilder) Connect(ctx context.Context) (*Conn, error) {
	switch {
	case b.conn.logger == nil:
		return nil, errors.New("postgres.Logger is no set")
	case b.address == "":
		return nil, errors.New("postgres.Address is no set")
	case b.database == "":
		return nil, errors.New("postgres.Database is no set")
	case b.conn.migrations != nil:
		if b.conn.migrationsTable == "" {
			return nil, errors.New("postgres.MigrationsTable is no set")
		}
	}

	_, log := b.conn.logger.Branch(ctx, "postgres")
	defer log.End()

	log.Debug().Msgf("connecting to %v/%s", b.address, b.database)

	connStr := fmt.Sprintf("postgres://%s:%s@%s/%s", b.username, b.password, b.address, b.database)

	pool, err := pgxpool.Connect(ctx, connStr)
	if err != nil {
		err = errors.WithMessage(err, "pgxpool.Connect")
		log.Error().Err(err).Msgf("failed connect to %s", b.database)
		return nil, err
	}
	b.conn.Pool = pool

	if err := pool.Ping(ctx); err != nil {
		err = errors.WithMessage(err, "pool.Ping")
		log.Error().Err(err).Msgf("failed ping to %s", b.database)
		return nil, err
	}

	log.Debug().Msg("connected")

	return b.conn, nil
}
