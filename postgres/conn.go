package postgres

import (
	"context"
	"embed"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"

	"gitlab.com/tehidev/go/monolog"
)

type Conn struct {
	*pgxpool.Pool

	logger monolog.Logger

	migrationsTable string

	migrations *embed.FS
}

func (conn *Conn) Close(ctx context.Context) {
	_, log := conn.logger.Branch(ctx, "postgres")
	defer log.End()

	log.Debug().Msg("connection closing")
	conn.Pool.Close()
	log.Debug().Msg("connection closed")
}

func (conn *Conn) Tx(ctx context.Context, inTransaction func(tx Tx) error) error {
	tx, err := conn.Pool.Begin(ctx)
	if err != nil {
		return errors.WithMessage(err, "pool.Begin")
	}
	if err = inTransaction(tx); err == nil {
		if err = tx.Commit(ctx); err != nil {
			return errors.WithMessage(err, "tx.Commit")
		}
		return nil
	}
	if rbErr := tx.Rollback(ctx); rbErr != nil {
		err = errors.WithMessage(err, rbErr.Error())
	}
	return err
}
