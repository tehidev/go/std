package postgres

import (
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
)

type (
	Tx   = pgx.Tx
	Row  = pgx.Row
	Rows = pgx.Rows

	Error = pgconn.PgError

	Scan interface {
		Scan(dest ...interface{}) error
	}
)

var (
	ErrNoRows = pgx.ErrNoRows

	ErrTxClosed         = pgx.ErrTxClosed
	ErrTxCommitRollback = pgx.ErrTxCommitRollback
)
