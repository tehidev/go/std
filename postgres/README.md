```go
package main

import (
    "context"
    "embed"
    "log"

    // "gitlab.com/tehidev/go/monolog"
    "gitlab.com/tehidev/go/std/postgres"
)

//go:embed migrations
var migrations embed.FS

func main() {
    ctx, cancel := context.WithCancel(context.TODO())
    defer cancel()

    conn, err := postgres.New().
        WithAddress("127.0.0.0:5432").
        WithDatabase("app").
        WithCredentials("username", "password").
        WithMigrationsTable("app_migrations").
        WithEmbeddedMigrations(&migrations).
        // WithLogger(monolog.Logger)
        Connect(ctx)
    check(err)
    defer conn.Close()

    err = conn.Migrate(ctx)
    check(err)

    targetNumber := 1
    err = conn.MigrateTo(ctx, targetNumber)
    check(err)

    err = conn.Tx(ctx, func(tx postgres.Tx) error {
        rows, err := tx.Query(ctx, "SELECT * FROM table")
        if err != nil {
            return err
        }
        defer rows.Close()
    })
    check(err)
}
```
