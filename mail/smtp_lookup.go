package mail

import (
	"net"
	"strings"
)

var knownSMTPServers = []string{
	"yandex.ru", "smtp.yandex.ru",
	".google.com", "smtp.gmail.com",
}

// Lookup main smtp server by service domain
func SMTPLookup(domain string) string {
	domain, ok := smtpLookupMX(domain)
	if !ok {
		if host, ok := smtpLookupHost("smtp." + domain); ok {
			return host
		}
		return domain
	}
	for i := 0; i < len(knownSMTPServers); i += 2 {
		if strings.HasSuffix(domain, knownSMTPServers[i]) {
			return knownSMTPServers[i+1]
		}
	}
	if host, ok := smtpLookupHost("smtp." + domain); ok {
		return host
	}
	return domain
}

func smtpLookupHost(host string) (string, bool) {
	addrs, _ := net.LookupHost(host)
	return host, len(addrs) != 0
}

func smtpLookupMX(host string) (string, bool) {
	mxs, err := net.LookupMX(host)
	if err != nil || len(mxs) == 0 {
		return host, false
	}
	host = strings.TrimSuffix(mxs[0].Host, ".")
	if strings.Count(host, ".") <= 2 {
		return host[strings.IndexByte(host, '.')+1:], true
	}
	return host, true
}
