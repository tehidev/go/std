package mail

import (
	"errors"
	"net/smtp"
	"strconv"
	"strings"
)

// ClientConfig for smtp sender
type ClientConfig struct {
	Server string `yaml:"server"` // smtp server, auto lookup by default
	Port   uint16 `yaml:""`       // possible: 25/2525 (unsafe), 587 (TLS), 465 (SSL) // 587 by default

	Username string `yaml:"username"` // required, usually email
	Password string `yaml:"password"` // optional, raw password

	FromMail string `yaml:"from_mail"` // optional, Username by default
	FromName string `yaml:"from_name"` // optional, Username prefix by default

	DefaultTo  []string `yaml:"default_to"`  // recipients
	DefaultCc  []string `yaml:"default_cc"`  // recipients, carbon copy
	DefaultBcc []string `yaml:"default_bcc"` // recipients, blind carbon copy

	SendQueueSize int `yaml:"send_queue_size"`

	from string    // "FromName <From>""
	auth smtp.Auth // plain
}

// ValidAndRepair smtp config
func (c *ClientConfig) ValidAndRepair() error {
	spaceReplacer := strings.NewReplacer(" ", "", "\t", "")

	c.Username = spaceReplacer.Replace(c.Username)
	if c.Username == "" {
		return errors.New("smtp.client config.username no set")
	}
	c.FromMail = spaceReplacer.Replace(c.FromMail)
	if c.FromMail == "" {
		c.FromMail = c.Username
	}
	c.FromName = strings.TrimSpace(c.FromName)
	if c.FromName == "" {
		if i := strings.LastIndex(c.FromMail, "@"); i != -1 {
			c.FromName = c.FromMail[:i]
		}
	}
	c.from = c.FromName + " <" + c.FromMail + ">"

	if c.SendQueueSize < 1 {
		c.SendQueueSize = 1000
	}
	if c.Port == 0 {
		c.Port = 587
	}
	if c.Server == "" {
		if i := strings.LastIndex(c.Username, "@"); i != -1 {
			c.Server = SMTPLookup(c.Username[i+1:])
		} else {
			return errors.New("smtp.client config.username invalid")
		}
	}

	host := c.Server
	if i := strings.IndexRune(c.Server, ':'); i != -1 {
		host = c.Server[:i]
	} else {
		c.Server += ":" + strconv.Itoa(int(c.Port))
	}
	c.auth = smtp.PlainAuth("", c.Username, c.Password, host)

	return nil
}
