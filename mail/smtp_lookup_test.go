package mail

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSMTPLookup(t *testing.T) {
	assert.Equal(t, "smtp.gmail.com", SMTPLookup("gmail.com"))
	assert.Equal(t, "smtp.google.com", SMTPLookup("google.com"))
	assert.Equal(t, "smtp.yandex.ru", SMTPLookup("yandex.ru"))
}
