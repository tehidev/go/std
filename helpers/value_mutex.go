package helpers

import "sync"

func NewValueMutex() *ValueMutex {
	return &ValueMutex{
		x: make(map[int64]*sync.Mutex),
	}
}

type ValueMutex struct {
	x  map[int64]*sync.Mutex
	mx sync.Mutex
}

func (vm *ValueMutex) Lock(value int64) (unlock func()) {
	vm.mx.Lock()

	currMutex, ok := vm.x[value]
	if !ok {
		currMutex = new(sync.Mutex)
		vm.x[value] = currMutex
	}

	vm.mx.Unlock()

	currMutex.Lock()

	unlock = func() {
		currMutex.Unlock()

		vm.mx.Lock()
		delete(vm.x, value)
		vm.mx.Unlock()
	}

	return
}
