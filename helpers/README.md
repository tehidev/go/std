```go
import "gitlab.com/tehidev/go/std/helpers"

func ToSnakeCase(s string) string

func GenerateTLSCert(bits int, organization string, maxAge time.Duration) (*tls.Certificate, error)
func GeneratePEMCert(bits int, organization string, maxAge time.Duration) (cert, key []byte, err error)

func NewDiscardLogger() *log.Logger

func FastRandomRead(buf []byte)
func FastRandomBytes(n int) []byte
func FastRandomString(n int) string

func SwapFileExtension(filename, targetExt string) string

func Permutation(s []string, walk func([]string))

func Workdir() string
func WorkdirDSN() string
func CallerFile(deep int) (path string, line int)
func CallerFilePath(deep int) string
func CallerFileName(deep int) string
func WaitForExit()
func RuntimePkgIndex(file, funcName string) int
func RuntimePkgPath() string
func RuntimePkgFileLineByPC(pc uintptr) (file string, line int)
func RuntimePkgFileLineStrByPC(pc uintptr) string

func ZAtob(s string) []byte
func ZBtoa(b []byte) string

func NewValueMutex() *ValueMutex
```
