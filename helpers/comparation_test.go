package helpers

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConstantTimeCompareStrings(t *testing.T) {
	assert.True(t, ConstantTimeCompareStrings("abc", "abc"))
	assert.False(t, ConstantTimeCompareStrings("abc", "abcd"))
	assert.False(t, ConstantTimeCompareStrings("abcd", "abc"))
}
