package helpers

import "math/rand"

const (
	randAlphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	randIdxBits  = 6                  // 6 bits to represent a letter index
	randIdxMask  = 1<<randIdxBits - 1 // All 1-bits, as many as letterIdxBits
	randIdxMax   = 63 / randIdxBits   // # of letter indices fitting in 63 bits
)

func FastRandomRead(buf []byte) {
	for i, cache, remain := 0, rand.Int63(), randIdxMax; i < len(buf); {
		if remain == 0 {
			cache, remain = rand.Int63(), randIdxMax
		}
		if idx := int(cache & randIdxMask); idx < len(randAlphabet) {
			buf[i] = randAlphabet[idx]
			i++
		}
		cache >>= randIdxBits
		remain--
	}
}

func FastRandomBytes(n int) []byte {
	b := make([]byte, n)
	if n > 0 {
		FastRandomRead(b)
	}
	return b
}

func FastRandomString(n int) string {
	return string(FastRandomBytes(n))
}
