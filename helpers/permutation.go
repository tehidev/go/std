package helpers

// Permutation calls "walk" with each permutation of s
func Permutation(s []string, walk func([]string)) {
	permutation(s, walk, 0)
}

// Permute the values at index i to len(s)-1
func permutation(s []string, walk func([]string), i int) {
	if i > len(s) {
		walk(s)
		return
	}
	permutation(s, walk, i+1)
	for j := i + 1; j < len(s); j++ {
		s[i], s[j] = s[j], s[i]
		permutation(s, walk, i+1)
		s[i], s[j] = s[j], s[i]
	}
}
