package helpers

import "path/filepath"

func SwapFileExtension(filename, targetExt string) string {
	ext := filepath.Ext(filename)
	filename = filename[:len(filename)-len(ext)]
	if targetExt[0] == '.' {
		return filename + targetExt
	}
	return filename + "." + targetExt
}
