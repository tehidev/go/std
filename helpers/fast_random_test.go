package helpers

import (
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func BenchmarkRandomBytesTo(t *testing.B) {
	buf := make([]byte, 64)
	for i := 0; i < t.N; i++ {
		FastRandomRead(buf)
	}
}

func BenchmarkRandomBytes(t *testing.B) {
	for i := 0; i < t.N; i++ {
		FastRandomBytes(64)
	}
}

func BenchmarkRandomString(t *testing.B) {
	for i := 0; i < t.N; i++ {
		FastRandomString(64)
	}
}

func TestRandomToken(t *testing.T) {
	size := 24
	seed := time.Now().UnixNano()

	rand.Seed(seed)
	tb := FastRandomBytes(size)
	rand.Seed(seed)
	ts := FastRandomString(size)

	assert.Equal(t, ts, string(tb))
}
