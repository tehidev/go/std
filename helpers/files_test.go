package helpers

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSwapFileExtension(t *testing.T) {
	assert.Equal(t, "file.txt", SwapFileExtension("file.doc", "txt"))
	assert.Equal(t, "file.txt", SwapFileExtension("file.doc", ".txt"))
	assert.Equal(t, "file.txt", SwapFileExtension("file.txt", "txt"))
}
