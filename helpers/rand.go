package helpers

import (
	"math/rand"
	"time"
)

func RandSeedByTime() {
	rand.Seed(time.Now().Unix())
}
