package helpers

import (
	"io"
	"log"
)

func NewDiscardLogger() *log.Logger {
	return log.New(io.Discard, "", 0)
}
