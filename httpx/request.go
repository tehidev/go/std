package httpx

import (
	"context"
	"io"
	"mime/multipart"
	"net"
	"net/http"
	"strings"
	"sync"

	jsoniter "github.com/json-iterator/go"
	"github.com/mssola/user_agent"
)

// Request net/http with context.Context, current route and ResponseWriter
type Request struct {
	*http.Request
	context.Context

	Response *Response

	Payload interface{}

	RouteVals []RouteValue
	RouteTail string
	RoutePath string

	formReader *FormReader
}

var requestPool = &sync.Pool{New: func() interface{} {
	return &Request{Response: new(Response), RouteVals: []RouteValue{}}
}}

// GetRequest from pool and fill
func GetRequest(w http.ResponseWriter, r *http.Request) *Request {
	rw := requestPool.Get().(*Request)

	rw.Request = r
	rw.Context = r.Context()

	rw.Response.ResponseWriter = w
	rw.Response.responseCode = 0
	rw.Response.responseSize = 0

	rw.Payload = nil

	rw.RouteVals = rw.RouteVals[:0]
	rw.RouteTail = ""
	rw.RoutePath = ""

	return rw
}

// PutRequest to pool
func PutRequest(r *Request) {
	if fr := r.formReader; fr != nil {
		r.formReader = nil
		PutFormReader(fr)
	}
	requestPool.Put(r)
}

// RouteStr value
func (r *Request) RouteStr(name string) string {
	for _, v := range r.RouteVals {
		if v.Name == name {
			return v.Raw
		}
	}
	return ""
}

// RouteInt value
func (r *Request) RouteInt(name string) int64 {
	for _, v := range r.RouteVals {
		if v.Name == name {
			return v.Int
		}
	}
	return 0
}

func (r *Request) IsGRPC() bool {
	return r.ProtoMajor == 2 && strings.HasPrefix(r.Header.Get(HeaderContentType), MimeApplicationGRPC)
}

// URI of request with query
func (r *Request) URI() string {
	if r.URL.RawQuery == "" {
		return r.URL.Path
	}
	return r.URL.Path + "?" + r.URL.RawQuery
}

// RealIP of request headers
func (r *Request) RealIP() string {
	if realIP := r.Header.Get(HeaderXRealIP); realIP != "" {
		return realIP
	}
	if realIP := r.Header.Get(HeaderXForwardedFor); realIP != "" {
		return strings.Split(realIP, ", ")[0]
	}
	realIP, _, _ := net.SplitHostPort(r.RemoteAddr)
	return realIP
}

// ParseJSON via easyjson or standard decoder
func (r *Request) ParseJSON(dst interface{}) error {
	defer r.Body.Close()
	dec := jsoniter.NewDecoder(r.Body)
	return dec.Decode(&dst)
}

// FormFile without opening or nil if not found
func (r *Request) FormFile(key string) *multipart.FileHeader {
	if form := r.MultipartForm; form != nil {
		if fhs := form.File[key]; len(fhs) > 0 {
			return fhs[0]
		}
	}
	return nil
}

// FormFiles without opening or nil if not found
func (r *Request) FormFiles(key string) []*multipart.FileHeader {
	if form := r.MultipartForm; form != nil {
		return form.File[key]
	}
	return nil
}

// FormFileData and header
func (r *Request) FormFileData(key string) ([]byte, *multipart.FileHeader) {
	if form := r.MultipartForm; form != nil {
		if fhs := form.File[key]; len(fhs) > 0 {
			if f, err := fhs[0].Open(); err == nil {
				defer f.Close()
				if data, err := io.ReadAll(f); err == nil {
					return data, fhs[0]
				}
			}
		}
	}
	return nil, nil
}

// FormReader requence
func (r *Request) FormReader() *FormReader {
	if r.formReader == nil {
		r.formReader = GetFormReader(r.Request)
	}
	return r.formReader
}

// UserAgent parse via github.com/mssola/user_agent
func (r *Request) UserAgent() *user_agent.UserAgent {
	return user_agent.New(r.Request.UserAgent())
}
