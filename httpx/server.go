package httpx

import (
	"context"
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/pkg/errors"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"

	"gitlab.com/tehidev/go/monolog"
	"gitlab.com/tehidev/go/std/helpers"
)

func NewServer() *ServerBuilder {
	s := &ServerBuilder{
		logger: monolog.New().
			WithWriter(os.Stdout).
			Build(),
		host: "0.0.0.0",
		port: 0,
		s: &http.Server{
			ReadTimeout:       time.Second * 20,
			ReadHeaderTimeout: time.Second * 20,
			WriteTimeout:      time.Second * 20,
			IdleTimeout:       time.Second * 5,
			ErrorLog:          helpers.NewDiscardLogger(),
		},
	}
	return s
}

type ServerBuilder struct {
	s *http.Server

	logger monolog.Logger

	host string
	port uint16

	certFile    string
	certKeyFile string

	certSelf bool
	certOrg  string
	certBits int
	certAge  time.Duration
}

func (b *ServerBuilder) WithPort(listenPort uint16) *ServerBuilder {
	b.port = listenPort
	return b
}

func (b *ServerBuilder) WithHost(listenHost string) *ServerBuilder {
	b.host = listenHost
	return b
}

func (b *ServerBuilder) WithHandler(h http.Handler) *ServerBuilder {
	b.s.Handler = h
	return b
}

func (b *ServerBuilder) WithTimeouts(readTimeout, writeTimeout, idleTimeout time.Duration) *ServerBuilder {
	b.s.ReadTimeout = readTimeout
	b.s.WriteTimeout = writeTimeout
	b.s.IdleTimeout = idleTimeout
	return b
}

func (b *ServerBuilder) WithReadHeaderTimeout(t time.Duration) *ServerBuilder {
	b.s.ReadHeaderTimeout = t
	return b
}

func (b *ServerBuilder) WithReadTimeout(t time.Duration) *ServerBuilder {
	b.s.ReadTimeout = t
	return b
}

func (b *ServerBuilder) WithWriteTimeout(t time.Duration) *ServerBuilder {
	b.s.WriteTimeout = t
	return b
}

func (b *ServerBuilder) WithIdleTimeout(t time.Duration) *ServerBuilder {
	b.s.IdleTimeout = t
	return b
}

func (b *ServerBuilder) WithTLSCerts(certFile, certKeyFile string) *ServerBuilder {
	b.certFile, b.certKeyFile = certFile, certKeyFile
	return b
}

func (b *ServerBuilder) WithSelfsignedTLS(organization string, bits int, age time.Duration) *ServerBuilder {
	b.certSelf = true
	b.certOrg = organization
	b.certBits = bits
	b.certAge = age
	return b
}

func (b *ServerBuilder) WithCustomServer(f func(*http.Server)) *ServerBuilder {
	f(b.s)
	return b
}

func (b *ServerBuilder) WithErrorLogger(logger *log.Logger) *ServerBuilder {
	b.s.ErrorLog = logger
	return b
}

func (b *ServerBuilder) WithLogger(logger monolog.Logger) *ServerBuilder {
	b.logger = logger
	return b
}

func (b *ServerBuilder) Start(ctx context.Context) (closer func(context.Context) error, err error) {
	switch {
	case b.logger == nil:
		return nil, errors.New("https.Server.Logger is no set")
	case b.s.Handler == nil:
		return nil, errors.New("https.Server.Handler is no set")
	}

	_, log := b.logger.Branch(ctx, "http_server")

	listenAddr := fmt.Sprintf("%s:%d", b.host, b.port)
	log.Debug().
		Str("listen_addr", listenAddr).
		Msg("open TCP socket")

	socket, err := net.Listen("tcp", listenAddr)
	if err != nil {
		err = errors.WithMessage(err, "net.Listen")
		log.Error().Err(err).
			Str("listen_addr", listenAddr).
			Msg("cannot to open listen tcp socket")
		log.End()
		return nil, err
	}

	listenAddr = socket.Addr().String()
	if strings.HasPrefix(listenAddr, "[::]") {
		listenAddr = "127.0.0.1" + listenAddr[len("[::]"):]
	}

	enableTLS := b.certSelf || b.certFile != "" || b.certKeyFile != ""

	if b.certSelf {
		selfSignedCert, err := helpers.GenerateTLSCert(b.certBits, b.certOrg, b.certAge)
		if err != nil {
			err = errors.WithMessage(err, "helpers.GenerateTLSCert")
			log.Error().Err(err).Msg("cannot to generate self-signed certificate")
			log.End()
			return nil, err
		}
		conf := b.s.TLSConfig
		if conf == nil {
			conf = &tls.Config{}
			b.s.TLSConfig = conf
		}
		conf.Certificates = append(conf.Certificates, *selfSignedCert)
	}

	begin := make(chan struct{})
	go func() {
		close(begin)
		if enableTLS {
			log.Debug().Msgf("serve https://%s", listenAddr)
			err = b.s.ServeTLS(socket, b.certFile, b.certKeyFile)
		} else {
			log.Debug().Msgf("serve http://%s", listenAddr)
			err = b.s.Serve(socket)
		}
		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Error().Err(err).Msg("listen error")
		} else {
			log.Debug().Msg("server closed")
		}
	}()
	<-begin

	closer = func(ctx context.Context) error {
		err := b.s.Shutdown(ctx)
		if err != nil {
			if errors.Is(err, http.ErrServerClosed) {
				log.Debug().Msg("server closed")
			} else {
				log.Warn().Err(err).Msg("closing failed")
			}
		}
		return errors.WithMessage(err, "http.Server.Shutdown")
	}

	return
}

func (b *ServerBuilder) Start2(ctx context.Context, h2 *http2.Server) (closer func(context.Context) error, err error) {
	if h2 == nil {
		h2 = &http2.Server{}
	}
	b.s.Handler = h2c.NewHandler(b.s.Handler, h2)
	return b.Start(ctx)
}
