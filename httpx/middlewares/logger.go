package middlewares

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/tehidev/go/monolog"
	"gitlab.com/tehidev/go/std/httpx"
)

type Logger struct{}

func (*Logger) Handler(next httpx.Handler) httpx.Handler {
	return func(r *httpx.Request) (err error) {
		requestRoute := r.RoutePath
		if len(requestRoute) == 0 {
			requestRoute = "404"
		}

		defer func() {
			if rec := recover(); rec != nil {
				if err, _ = rec.(error); err == nil {
					err = errors.New(fmt.Sprint(rec))
				}
			}

			if err != nil {
				r.Response.Status(http.StatusInternalServerError)
			}

			status := r.Response.ResponseCode()
			method := r.Method
			if r.IsGRPC() {
				method = "GRPC"
			}

			if err == nil {
				log.Printf("[%d] %s %s", status, method, r.URI())
			} else {
				log.Printf("[%d] %s %s : %s", status, method, r.URI(), err)
			}
		}()

		return next(r)
	}
}

type MonoLogger struct {
	Logger monolog.Logger

	RequestInfo bool
}

func (m *MonoLogger) Handler(next httpx.Handler) httpx.Handler {
	return func(r *httpx.Request) (err error) {
		requestRoute := r.RoutePath
		if len(requestRoute) == 0 {
			requestRoute = "404"
		}

		ctx, span := m.Logger.Branch(r.Context, "http_request")
		defer span.End()
		r.Context = ctx

		if m.RequestInfo {
			span.Debug().
				Str("uri", r.URI()).
				Str("method", r.Method).
				Str("route", requestRoute).
				Str("remote_addr", r.RemoteAddr).
				Str("real_ip", r.RealIP()).
				Int64("content_length", r.ContentLength).
				Msg("request info")
		}

		defer func() {
			var event monolog.Event

			if rec := recover(); rec != nil {
				if err, _ = rec.(error); err == nil {
					err = errors.New(fmt.Sprint(rec))
				}
				event = span.Panic()
			} else if err == nil {
				event = span.Debug()
			} else {
				event = span.Error()
			}

			if err != nil {
				r.Response.Status(http.StatusInternalServerError)
			}

			status := r.Response.ResponseCode()
			method := r.Method
			if r.IsGRPC() {
				method = "GRPC"
			}

			event.
				Err(err).
				Int("response_code", status).
				Int("response_size", r.Response.ResponseSize()).
				Msgf("%s %d %s", method, status, r.URI())
		}()

		err = next(r)
		return
	}
}
