package middlewares

import "gitlab.com/tehidev/go/std/httpx"

type BasicAuth struct {
	Resolve func(username, password string) bool
}

func (auth *BasicAuth) Handler(nextHandler httpx.Handler) httpx.Handler {
	if auth.Resolve == nil {
		panic("resolve func no set")
	}
	return func(r *httpx.Request) error {
		r.Response.Header().Set(httpx.HeaderWWWAuthenticate, `Basic realm="Restricted"`)

		user, pass, ok := r.BasicAuth()
		if !ok || !auth.Resolve(user, pass) {
			return r.Response.StatusUnauthorized()
		}

		return nextHandler(r)
	}
}
