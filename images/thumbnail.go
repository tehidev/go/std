package images

import (
	"bytes"
	"image/jpeg"
	"image/png"
	"io"

	"github.com/nfnt/resize"
)

func WriteJPGThumbnail(src io.Reader, dst io.Writer, filename string, maxWidth, maxHeight uint) error {
	img, err := Read(filename, src)
	if err != nil {
		return err
	}
	resized := resize.Thumbnail(maxWidth, maxHeight, img, resize.Bicubic)
	return jpeg.Encode(dst, resized, nil)
}

func JPGThumbnail(src io.Reader, filename string, maxWidth, maxHeight uint) ([]byte, error) {
	var buf bytes.Buffer
	if err := WriteJPGThumbnail(src, &buf, filename, maxWidth, maxHeight); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func WritePNGThumbnail(src io.Reader, dst io.Writer, filename string, maxWidth, maxHeight uint) error {
	img, err := Read(filename, src)
	if err != nil {
		return err
	}
	resized := resize.Thumbnail(maxWidth, maxHeight, img, resize.Bicubic)
	return png.Encode(dst, resized)
}

func PNGThumbnail(src io.Reader, filename string, maxWidth, maxHeight uint) ([]byte, error) {
	var buf bytes.Buffer
	if err := WritePNGThumbnail(src, &buf, filename, maxWidth, maxHeight); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
