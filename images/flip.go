package images

import "image"

func YFlipRGBA(img *image.RGBA) {
	w, h := img.Bounds().Dx(), img.Bounds().Dy()

	var swap [4]byte

	for x := 0; x < w; x++ {
		for y := 0; y < h/2; y++ {
			o1, o2 := img.PixOffset(x, y), img.PixOffset(x, h-y-1)

			pix1 := img.Pix[o1 : o1+4 : o1+4]
			pix2 := img.Pix[o2 : o2+4 : o2+4]

			copy(swap[:], pix1)
			copy(pix1, pix2)
			copy(pix2, swap[:])
		}
	}
}
