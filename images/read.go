package images

import (
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/image/bmp"
)

func Read(filename string, src io.Reader) (image.Image, error) {
	fileFormat := strings.ToLower(filepath.Ext(filename))
	switch fileFormat {
	case ".jpg", ".jpeg":
		return jpeg.Decode(src)
	case ".png":
		return png.Decode(src)
	case ".bmp":
		return bmp.Decode(src)
	case ".gif":
		return gif.Decode(src)
	default:
		return nil, image.ErrFormat
	}
}

func ReadFromFile(filepath string) (image.Image, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	return Read(filepath, file)
}
