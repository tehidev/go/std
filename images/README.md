```go
import "gitlab.com/tehidev/go/std/images"

func Read(filename string, src io.Reader) (image.Image, error)
func ReadFromFile(filepath string) (image.Image, error)

func WriteJPGThumbnail(src io.Reader, dst io.Writer, filename string, maxWidth, maxHeight uint) error
func JPGThumbnail(src io.Reader, filename string, maxWidth, maxHeight uint) ([]byte, error)
func WritePNGThumbnail(src io.Reader, dst io.Writer, filename string, maxWidth, maxHeight uint) error
func PNGThumbnail(src io.Reader, filename string, maxWidth, maxHeight uint) ([]byte, error)
```
