```go
package main

import (
	"context"
	"os"
	"strings"

	"gitlab.com/tehidev/go/std/fstore"
)

func main() {
	defer os.RemoveAll("media")

	client, err := fstore.Open(fstore.Config{
		S3Mode:        false,
		RandPrefixLen: 4,
		LocalDir:      "media",
		LocalURL:      "/media",
	})
    check(err)

	r := strings.NewReader("avatar data")
	link, err := client.Put(context.TODO(), "avatars", "avatar.jpg", r.Size(), r)
    check(err)
    println(link)

	r.Reset("avatar 2 data")
	link, err = client.Put(context.TODO(), "", "avatar.jpg", r.Size(), r)
    check(err)
    println(link)
}
```
