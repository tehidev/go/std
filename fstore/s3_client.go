package fstore

import (
	"context"
	"io"
	"mime"
	"path/filepath"

	"github.com/pkg/errors"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"

	"gitlab.com/tehidev/go/std/helpers"
)

type s3Client struct {
	*Config
	c *minio.Client
}

func openS3Client(conf *Config) (minClient, error) {
	client, err := minio.New(conf.S3EndPoint, &minio.Options{
		Region: conf.S3Region,
		Secure: !conf.S3Insecure,
		Creds:  credentials.NewStaticV4(conf.S3AccessKey, conf.S3SecretKey, conf.S3AuthToken),
	})
	if err != nil {
		return nil, errors.WithMessage(err, "minio.New")
	}
	return &s3Client{Config: conf, c: client}, nil
}

func (c *s3Client) Put(
	ctx context.Context, category, filename string,
	size int64, body io.Reader) (link string, err error) {

	var randSlug string
	if c.RandPrefixLen > 0 {
		randSlug = helpers.FastRandomString(c.RandPrefixLen) + "/"
	}

	if category == "" {
		filename = randSlug + filepath.Base(filename)
	} else {
		filename = category + "/" + randSlug + filepath.Base(filename)
	}
	ctype := mime.TypeByExtension(filepath.Ext(filename))
	if ctype == "" {
		ctype = "application/octet-stream"
	}

	opt := minio.PutObjectOptions{ContentType: ctype}
	if _, err = c.c.PutObject(ctx, c.S3Bucket, filename, body, size, opt); err != nil {
		return "", errors.WithMessage(err, "fstore.PutObject")
	}
	return c.S3Domain + c.S3Bucket + "/" + filename, nil
}
