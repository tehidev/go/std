package fstore

import (
	"strings"

	"github.com/pkg/errors"
)

// Config for local storage or s3 connection
type Config struct {
	RandPrefixLen int `yaml:"rand_prefix_len"` // rand file prefix for safe

	S3Mode bool `yaml:"s3_mode"` // enable s3 storage for files

	LocalDir string `yaml:"local_dir"` // local storage directory. Default "media"
	LocalURL string `yaml:"local_url"` // target url file prefix. Default "/media/"

	S3AccessKey string `yaml:"s3_access_key"` // access key or login or user, for example: 32423_goservice
	S3SecretKey string `yaml:"s3_secret_key"` // secret key or password
	S3AuthToken string `yaml:"s3_auth_token"` // optional

	S3EndPoint string `yaml:"s3_end_point"` // provider api url, for example: s3.selcdn.ru
	S3Domain   string `yaml:"s3_domain"`    // storage domain, for example: https://56756.selcdn.ru
	S3Region   string `yaml:"s3_region"`    // for example: ru-1a
	S3Bucket   string `yaml:"s3_bucket"`    // service bucket root, for example: goservice
	S3Insecure bool   `yaml:"s3_insecure"`
}

func (c *Config) ValidAndRepair() error {
	if c.RandPrefixLen < 1 {
		c.RandPrefixLen = 0
	}
	if c.S3Mode {
		return c.validAndRepairS3()
	}
	c.repairLocal()
	return nil
}

func (c *Config) repairLocal() {
	if c.LocalDir == "" {
		c.LocalDir = "media"
	}
	if c.LocalURL == "" {
		c.LocalURL = "/media/"
	}
	c.LocalURL = "/" + strings.Trim(c.LocalURL, "/") + "/"
}

func (c *Config) validAndRepairS3() error {
	c.S3Bucket = strings.Trim(strings.TrimSpace(c.S3Bucket), "/")
	switch {
	case c.S3AccessKey == "":
		return errors.New("fstore.s3_access_key is no set")
	case c.S3SecretKey == "":
		return errors.New("fstore.s3_secret_key is no set")
	case c.S3EndPoint == "":
		return errors.New("fstore.s3_end_point is no set")
	case c.S3Domain == "":
		return errors.New("fstore.s3_domain is no set")
	case c.S3Region == "":
		return errors.New("fstore.s3_region is no set")
	case c.S3Bucket == "":
		return errors.New("fstore.s3_bucket is no set")
	}
	c.S3Domain = strings.Trim(c.S3Domain, "/") + "/"
	return nil
}
