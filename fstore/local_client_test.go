package fstore_test

import (
	"context"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/tehidev/go/std/fstore"
)

func TestLocalClient(t *testing.T) {
	defer os.RemoveAll("media")

	client, err := fstore.Open(fstore.Config{
		S3Mode:        false,
		RandPrefixLen: 4,
		LocalDir:      "media",
		LocalURL:      "/media",
	})
	assert.NoError(t, err)

	r := strings.NewReader("avatar data")
	link, err := client.Put(context.Background(), "avatars", "avatar.jpg", r.Size(), r)
	assert.NoError(t, err)
	assert.True(t, strings.HasPrefix(link, "/media/avatars/"))
	assert.True(t, strings.HasSuffix(link, "/avatar.jpg"))
	assert.Equal(t, 30, len(link))

	r.Reset("avatar 2 data")
	link, err = client.Put(context.Background(), "", "avatar.jpg", r.Size(), r)
	assert.NoError(t, err)
	assert.True(t, strings.HasPrefix(link, "/media/"))
	assert.True(t, strings.HasSuffix(link, "/avatar.jpg"))
	assert.Equal(t, 22, len(link))
}
