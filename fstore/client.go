package fstore

import (
	"context"
	"image"
	"io"
	"mime/multipart"

	"github.com/pkg/errors"

	"gitlab.com/tehidev/go/monolog"
)

// Client of S3 storage
//
// For opening use func Open
type Client interface {
	SetLogger(monolog.Logger)
	// Put returns link {Domain}/{Bucket}/{Category}/{RandPrefix}/{Filename}
	Put(ctx context.Context, category, filename string, size int64, body io.Reader) (link string, err error)
	// PutFile is alias Put with opening local file
	PutFile(ctx context.Context, category, filepath string) (link string, err error)
	// PutData is alias Put
	PutData(ctx context.Context, category, filename string, data []byte) (link string, err error)
	// PutImage ...
	PutImage(
		ctx context.Context, category, filename string, img image.Image, typ ImageType) (link string, err error)
	// PutMultipartThumbnail ...
	PutMultipartThumbnail(
		ctx context.Context, category string,
		file *multipart.FileHeader, typ ImageType, maxPixelSize uint) (link string, err error)
}

// Open local or s3 storage
//
// required fields:
//
//  for remote storage: AccessKey, SecretKey, Region, EndPoint, Domain, Bucket
//
//  for local storage: Local, LocalDir, LocalURL, Bucket (optional)
//
func Open(conf Config) (Client, error) {
	err := conf.ValidAndRepair()
	if err != nil {
		return nil, errors.WithMessage(err, "conf.ValidAndRepair")
	}

	var client minClient
	if conf.S3Mode {
		client, err = openS3Client(&conf)
		err = errors.WithMessage(err, "openS3Client")
	} else {
		client, err = openLocalClient(&conf)
		err = errors.WithMessage(err, "openLocalClient")
	}
	if err != nil {
		return nil, err
	}

	return newClientWrapper(client), nil
}
