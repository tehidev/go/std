package fstore

import (
	"context"
	"io"
	"os"
	"path/filepath"

	"github.com/pkg/errors"

	"gitlab.com/tehidev/go/std/helpers"
)

type localClient struct {
	*Config
}

func openLocalClient(conf *Config) (minClient, error) {
	return &localClient{Config: conf}, nil
}

func (c *localClient) Put(
	ctx context.Context, category, filename string,
	size int64, body io.Reader) (link string, err error) {

	dir, path, link := c.getDirPathLink(filename, category)

	if err = os.MkdirAll(dir, os.ModePerm); err != nil {
		return "", errors.WithMessage(err, "os.MkdirAll")
	}
	f, err := os.Create(path)
	if err != nil {
		return "", errors.WithMessage(err, "os.Create")
	}
	defer f.Close()

	_, err = io.Copy(f, body)
	return link, errors.WithMessage(err, "io.Copy")
}

func (c *localClient) getDirPathLink(filename, category string) (dir, path, link string) {
	filename = filepath.Base(filename)
	token := helpers.FastRandomString(c.RandPrefixLen)

	dir, link = c.LocalDir, c.LocalURL
	if category != "" {
		dir = filepath.Join(dir, category)
		link += category + "/"
	}

	if len(token) > 0 {
		dir = filepath.Join(dir, token)
		link += token + "/"
	}
	path = filepath.Join(dir, filename)
	link += filename

	return
}
