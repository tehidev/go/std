package fstore

import (
	"bytes"
	"context"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"mime/multipart"
	"os"

	"github.com/pkg/errors"

	"gitlab.com/tehidev/go/monolog"
	"gitlab.com/tehidev/go/std/helpers"
	"gitlab.com/tehidev/go/std/images"
)

type minClient interface {
	Put(ctx context.Context, category, filename string, size int64, body io.Reader) (link string, err error)
}

type clientWrapper struct {
	minClient

	logger monolog.Logger
}

func newClientWrapper(client minClient) *clientWrapper {
	return &clientWrapper{
		minClient: client,
		logger: monolog.New().
			WithWriter(os.Stdout).
			Build(),
	}
}

func (s *clientWrapper) SetLogger(logger monolog.Logger) {
	s.logger = logger
}

func (s *clientWrapper) put(
	ctx context.Context, log monolog.Log, category, filename string,
	size int64, body io.Reader) (link string, err error) {

	defer log.End()

	log.Debug().
		Str("file_category", category).
		Str("file_name", filename).
		Int64("file_size", size).
		Msg("upload file begin")

	link, err = s.minClient.Put(ctx, category, filename, size, body)

	if err == nil {
		log.Debug().Str("remote_link", link).Msgf("success: %s", link)
	} else {
		log.Error().Err(err).Msgf("upload file `%s`/`%s` (%d bytes) failed", category, filename, size)
	}
	return
}

func (s *clientWrapper) Put(
	ctx context.Context, category, filename string,
	size int64, body io.Reader) (link string, err error) {

	ctx, log := s.logger.Branch(ctx, "fstore: upload body")
	link, err = s.put(ctx, log, category, filename, size, body)
	err = errors.WithMessage(err, "fstore.Put")
	return
}

func (s *clientWrapper) PutFile(ctx context.Context, category, filepath string) (link string, err error) {
	ctx, log := s.logger.Branch(ctx, "fstore: upload file")

	f, err := os.Open(filepath)
	if err != nil {
		err = errors.WithMessagef(err, "open local file '%s'", filepath)
		log.Error().Err(err).Msgf("open local file `%s` failed", filepath)
		log.End()
		return "", err
	}
	defer f.Close()

	finfo, err := f.Stat()
	if err != nil {
		err = errors.WithMessagef(err, "open local file '%s'", filepath)
		log.Error().Err(err).Msgf("open local file `%s` failed", filepath)
		log.End()
		return "", err
	}

	link, err = s.put(ctx, log, category, finfo.Name(), finfo.Size(), f)
	err = errors.WithMessage(err, "fstore.PutFile")
	return
}

func (s *clientWrapper) PutData(
	ctx context.Context, category, filename string, data []byte) (link string, err error) {

	ctx, log := s.logger.Branch(ctx, "media: upload bytes")

	link, err = s.put(ctx, log, category, filename, int64(len(data)), bytes.NewReader(data))
	err = errors.WithMessage(err, "fstore.PutData")
	return
}

type ImageType int

const (
	ImageJPG ImageType = iota
	ImagePNG
)

func (s *clientWrapper) PutImage(
	ctx context.Context, category, filename string, img image.Image, typ ImageType) (link string, err error) {

	ctx, log := s.logger.Branch(ctx, "fstore: upload image")

	var buf bytes.Buffer
	switch typ {
	case ImagePNG:
		err = errors.WithMessage(png.Encode(&buf, img), "png.Encode")
		filename = helpers.SwapFileExtension(filename, "png")
	default:
		err = errors.WithMessage(jpeg.Encode(&buf, img, nil), "jpeg.Encode")
		filename = helpers.SwapFileExtension(filename, "jpg")
	}
	if err != nil {
		log.Error().Err(err).Msgf("read image file `%s` failed", filename)
		log.End()
		return "", errors.WithMessagef(err, "read image file `%s` failed", filename)
	}

	link, err = s.put(ctx, log, category, filename, int64(buf.Len()), &buf)
	err = errors.WithMessage(err, "fstore.PutImage")
	return
}

func (s *clientWrapper) PutMultipartThumbnail(
	ctx context.Context, category string,
	file *multipart.FileHeader, typ ImageType, maxPixelSize uint) (link string, err error) {

	ctx, log := s.logger.Branch(ctx, "fstore: upload multipart thumbnail file")

	f, err := file.Open()
	if err != nil {
		err = errors.WithMessagef(err, "read multipart file `%s`/`%s` failed", category, file.Filename)
		log.Error().Err(err).Msgf("read multipart file `%s`/`%s` failed", category, file.Filename)
		log.End()
		return "", err
	}
	defer f.Close()

	var imgData []byte
	var filename string
	switch typ {
	case ImagePNG:
		imgData, err = images.PNGThumbnail(f, file.Filename, maxPixelSize, maxPixelSize)
		err = errors.WithMessage(err, "images.PNGThumbnail")
		filename = helpers.SwapFileExtension(file.Filename, "png")
	default:
		imgData, err = images.JPGThumbnail(f, file.Filename, maxPixelSize, maxPixelSize)
		err = errors.WithMessage(err, "images.JPGThumbnail")
		filename = helpers.SwapFileExtension(file.Filename, "jpg")
	}
	if err != nil {
		err = errors.WithMessagef(err, "read multipart file `%s`/`%s` failed", category, file.Filename)
		log.Error().Err(err).Msgf("read multipart file `%s`/`%s` failed", category, file.Filename)
		log.End()
		return "", err
	}

	link, err = s.put(ctx, log, category, filename, int64(len(imgData)), bytes.NewReader(imgData))
	err = errors.WithMessage(err, "fstore.PutMultipartThumbnail")
	return
}
