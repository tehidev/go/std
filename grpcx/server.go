package grpcx

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"strings"

	"github.com/pkg/errors"
	"google.golang.org/grpc"

	"gitlab.com/tehidev/go/monolog"
)

type ServerBuilder struct {
	logger monolog.Logger

	srv  *grpc.Server
	host string
	port uint16
}

func NewServer() *ServerBuilder {
	return &ServerBuilder{
		logger: monolog.New().
			WithWriter(os.Stdout).
			Build(),
		srv:  grpc.NewServer(),
		host: "0.0.0.0",
		port: 0,
	}
}

func (b *ServerBuilder) Server() *grpc.Server {
	return b.srv
}

func (b *ServerBuilder) WithHost(host string) *ServerBuilder {
	b.host = host
	return b
}

func (b *ServerBuilder) WithPort(port uint16) *ServerBuilder {
	b.port = port
	return b
}

func (b *ServerBuilder) WithLogger(logger monolog.Logger) *ServerBuilder {
	b.logger = logger
	return b
}

func (b *ServerBuilder) RegisterService(desc *grpc.ServiceDesc, impl interface{}) {
	b.srv.RegisterService(desc, impl)
}

func (b *ServerBuilder) Start(ctx context.Context) (closer func(), err error) {
	if b.logger == nil {
		return nil, errors.New("grpc.Logger is no set")
	}

	_, log := b.logger.Branch(ctx, "grpc_server")

	listenAddr := fmt.Sprintf("%s:%d", b.host, b.port)
	log.Debug().
		Str("listen_addr", listenAddr).
		Msg("open TCP socket")

	socket, err := net.Listen("tcp", listenAddr)
	if err != nil {
		err = errors.WithMessage(err, "net.Listen")
		log.Error().Err(err).
			Str("listen_addr", listenAddr).
			Msg("cannot to open listen tcp socket")
		log.End()
		return nil, err
	}

	listenAddr = socket.Addr().String()
	if strings.HasPrefix(listenAddr, "[::]") {
		listenAddr = "127.0.0.1" + listenAddr[len("[::]"):]
	}

	begin := make(chan struct{})
	go func() {
		close(begin)

		log.Debug().Msgf("serve grpc://%s", listenAddr)
		err = b.srv.Serve(socket)

		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Error().Err(err).Msg("listen error")
		} else {
			log.Debug().Msg("server closed")
		}
	}()
	<-begin

	closer = func() {
		b.srv.GracefulStop()
		log.Debug().Msg("server closed")
	}

	return
}
