```go

import "gitlab.com/tehidev/go/grpcx"

func main() {
	grpcServer := grpcx.NewServer().
		WithPort(conf.Linker.GRPCPort)
        //WithHost("0.0.0.0").
		//WithLogger(logger)

	pb.RegisterLinkerServer(grpcServer, newGRPCService())

	grpcServerClose, err := grpcServer.Start(ctx)
	check(err, "grpcServer.Start")
	defer grpcServerClose()
}

```
