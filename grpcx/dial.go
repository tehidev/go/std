package grpcx

import (
	"context"
	"crypto/tls"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

func InsecureDialContext(ctx context.Context, addr string) (*grpc.ClientConn, error) {
	return grpc.DialContext(ctx, addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
}

func TLSDialContext(ctx context.Context, addr string, cfg *tls.Config) (*grpc.ClientConn, error) {
	if cfg == nil {
		cfg = &tls.Config{}
	}
	return grpc.DialContext(ctx, addr, grpc.WithTransportCredentials(credentials.NewTLS(cfg)))
}
