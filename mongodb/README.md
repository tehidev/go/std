```go
package main

import (
    "context"
    "log"

    "gitlab.com/tehidev/go/std/mongodb"
)

func main() {
    ctx, cancel := context.WithCancel(context.TODO())
    defer cancel()

    conn, err := mongodb.New().
        WithHost("127.0.0.1:27017").
        WithHost("127.0.0.2", "127.0.0.3:27017").
        WithDatabase("app").
        WithCredentials("username", "password").
        WithMigrationsCollection("app_migrations").
        // WithLogger(monolog.Logger)
        WithMigrations(map[int]mongodb.Migration{
            1: {
                Forward: func(c *mongodb.Conn) error { return nil },
                Reverse: func(c *mongodb.Conn) error { return nil },
            },
            2: {
                Forward: func(c *mongodb.Conn) error { return nil },
                Reverse: func(c *mongodb.Conn) error { return nil },
            },
        }).
        Connect(ctx)
    check(err)
    defer conn.Close(ctx)

    err = conn.Migrate(ctx)
    check(err)

    targetNumber := 1
    err = conn.MigrateTo(ctx, targetNumber)
    check(err)

    var results []interface{}
    err = conn.FindAll(ctx, "test_collection", &results, mongodb.M{
        "a": "b",
    })
    check(err)
}
```
