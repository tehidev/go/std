package mongodb

import (
	"context"
	"os"
	"strings"

	"github.com/pkg/errors"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/tehidev/go/monolog"
)

func New() *ConnectionBuilder {
	return &ConnectionBuilder{
		conn: &Conn{
			migrationsCol: "app_migrations",
			logger: monolog.New().
				WithWriter(os.Stdout).
				Build(),
		},
		hosts:    []string{},
		database: "postgres",
		username: "postgres",
		password: "",
	}
}

type ConnectionBuilder struct {
	conn *Conn

	hosts    []string
	database string

	username string
	password string
}

func (b *ConnectionBuilder) WithLogger(logger monolog.Logger) *ConnectionBuilder {
	b.conn.logger = logger
	return b
}

func (b *ConnectionBuilder) WithHost(hosts ...string) *ConnectionBuilder {
	for _, host := range hosts {
		if !strings.ContainsRune(host, ':') {
			host += ":27017"
		}
		b.hosts = append(b.hosts, host)
	}
	return b
}

func (b *ConnectionBuilder) WithDatabase(dbName string) *ConnectionBuilder {
	b.database = dbName
	return b
}

func (b *ConnectionBuilder) WithCredentials(username, password string) *ConnectionBuilder {
	b.username = username
	b.password = password
	return b
}

func (b *ConnectionBuilder) WithMigrationsCollection(collectionName string) *ConnectionBuilder {
	b.conn.migrationsCol = collectionName
	return b
}

func (b *ConnectionBuilder) WithMigrations(migrations map[int]Migration) *ConnectionBuilder {
	b.conn.migrations = migrations
	return b
}

func (b *ConnectionBuilder) Connect(ctx context.Context) (*Conn, error) {
	switch {
	case b.conn.logger == nil:
		return nil, errors.New("mongodb.Logger is no set")
	case len(b.hosts) == 0:
		return nil, errors.New("mongodb.Hosts is no set")
	case b.database == "":
		return nil, errors.New("mongodb.Database is no set")
	case b.conn.migrations != nil:
		if b.conn.migrationsCol == "" {
			return nil, errors.New("mongodb.MigrationsCollection is no set")
		}
	}

	opts := &options.ClientOptions{
		Auth: &options.Credential{
			Username:    b.username,
			Password:    b.password,
			PasswordSet: b.password != "",
		},
		Hosts: b.hosts,
	}

	_, log := b.conn.logger.Branch(ctx, "mongodb")
	defer log.End()

	log.Debug().Msgf("connecting to %v/%s@%s", b.hosts, b.database, b.username)

	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		err = errors.WithMessage(err, "mongo.Connect")
		log.Error().Err(err).Msgf("failed connect to %s", b.database)
		return nil, err
	}

	if err = client.Ping(ctx, nil); err != nil {
		err = errors.WithMessage(err, "client.Ping")
		log.Error().Err(err).Msgf("failed ping to %s", b.database)
		return nil, err
	}

	log.Debug().Msg("connected")

	b.conn.db = client.Database(b.database)

	return b.conn, nil
}
