package mongodb

import (
	"github.com/pkg/errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// ==================================================================

func (conn *Conn) InsertOne(ctx Context, collection string, document interface{}, returnID *PK) error {
	result, err := conn.db.Collection(collection).InsertOne(ctx, document)
	if err != nil {
		return errors.WithMessage(err, "InsertOne")
	}
	if returnID != nil {
		objID := result.InsertedID.(primitive.ObjectID)
		*returnID = objID.Hex()
	}
	return nil
}

// ==================================================================

func (conn *Conn) UpdateOne(ctx Context, collection string, filter M, update interface{}) error {
	if err := fixFilterID(filter); err != nil {
		return err
	}
	_, err := conn.db.Collection(collection).UpdateOne(ctx, filter, update)
	return errors.WithMessage(err, "UpdateOne")
}

func (conn *Conn) UpdateMany(ctx Context, collection string, filter M, update interface{}) error {
	if err := fixFilterID(filter); err != nil {
		return err
	}
	_, err := conn.db.Collection(collection).UpdateMany(ctx, filter, update)
	return errors.WithMessage(err, "UpdateMany")
}

func (conn *Conn) UpdateByID(ctx Context, collection string, id PK, update interface{}) error {
	filter := bson.M{"_id": id}
	return conn.UpdateOne(ctx, collection, filter, update)
}

// ==================================================================

func (conn *Conn) DeleteOne(ctx Context, collection string, filter M) error {
	if err := fixFilterID(filter); err != nil {
		return err
	}
	_, err := conn.db.Collection(collection).DeleteOne(ctx, filter)
	return errors.WithMessage(err, "DeleteOne")
}

func (conn *Conn) DeleteByID(ctx Context, collection string, id PK) error {
	filter := bson.M{"_id": id}
	return conn.DeleteOne(ctx, collection, filter)
}

func (conn *Conn) DeleteMany(ctx Context, collection string, filter M) error {
	if err := fixFilterID(filter); err != nil {
		return err
	}
	_, err := conn.db.Collection(collection).DeleteMany(ctx, filter)
	return errors.WithMessage(err, "DeleteMany")
}

// ==================================================================

func (conn *Conn) FindOne(ctx Context, collection string, filter M, dst interface{}, opts ...*options.FindOneOptions) (ok bool, err error) {
	if err := fixFilterID(filter); err != nil {
		return false, err
	}

	err = conn.db.Collection(collection).FindOne(ctx, filter, opts...).Decode(dst)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return false, nil
		}
		return false, errors.WithMessage(err, "Decode")
	}
	return true, nil
}

func (conn *Conn) FindByID(ctx Context, collection string, id PK, dst interface{}) (ok bool, err error) {
	filter := bson.M{"_id": id}
	return conn.FindOne(ctx, collection, filter, dst)
}

func (conn *Conn) FindAll(ctx Context, collection string, dst interface{}, filter M, opts ...*options.FindOptions) error {
	cursor, err := conn.db.Collection(collection).Find(ctx, filter, opts...)
	if err != nil {
		return errors.WithMessage(err, "Find")
	}
	err = cursor.All(ctx, dst)
	return errors.WithMessage(err, "cursor.All")
}

func (conn *Conn) Exists(ctx Context, collection string, filter M) (ok bool, err error) {
	result := conn.db.Collection(collection).FindOne(ctx, filter)
	switch result.Err() {
	case mongo.ErrNoDocuments:
		return false, nil
	case nil:
		return true, nil
	default:
		return false, errors.WithMessage(result.Err(), "FindOne")
	}
}

// ==================================================================

func (conn *Conn) CountDocuments(ctx Context, collection string, filter M) (int64, error) {
	return conn.db.Collection(collection).CountDocuments(ctx, filter)
}

// ==================================================================

func (conn *Conn) CreateCollection(ctx Context, collection string, opts ...*options.CreateCollectionOptions) error {
	if err := conn.db.CreateCollection(ctx, collection); err != nil {
		return errors.WithMessage(err, "CreateCollection")
	}
	return nil
}

func (conn *Conn) CreateIndexes(ctx Context, collection string, opts []mongo.IndexModel) error {
	_, err := conn.db.Collection(collection).Indexes().CreateMany(ctx, opts)
	return errors.WithMessage(err, "CreateMany")
}

// ==================================================================

func fixFilterID(filter M) error {
	if id, ok := filter["_id"].(string); ok {
		id, err := primitive.ObjectIDFromHex(id)
		if err != nil {
			return errors.WithMessage(err, "primitive.ObjectIDFromHex(\"_id\")")
		}
		filter["_id"] = id
	}
	return nil
}
