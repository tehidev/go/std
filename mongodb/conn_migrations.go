package mongodb

import (
	"time"

	"go.mongodb.org/mongo-driver/mongo/options"
)

type Migration struct {
	Forward func(*Conn) error
	Reverse func(*Conn) error
}

type migrationDoc struct {
	ID PK `bson:"_id,omitempty" json:"id"`

	Created time.Time `bson:"created_at" json:"createdAt"`

	Number int `bson:"number" json:"number"`
}

func (conn *Conn) Migrate(ctx Context) error {
	return conn.MigrateTo(ctx, 99999999)
}

func (conn *Conn) MigrateTo(ctx Context, targetNumber int) error {
	_, log := conn.logger.Branch(ctx, "mongodb")
	defer log.End()

	log.Debug().Msg("run migrations")

	_ = conn.CreateCollection(ctx, conn.migrationsCol)

	var maxMigration migrationDoc
	var currNumber int
	opts := []*options.FindOneOptions{
		{Sort: M{"number": -1 /* descending */}},
	}
	ok, err := conn.FindOne(ctx, conn.migrationsCol, M{}, &maxMigration, opts...)
	if err != nil {
		return err
	}
	if ok {
		currNumber = maxMigration.Number
	}

	log.Debug().Msgf("current migration number: %d", currNumber)

	if targetNumber < currNumber {
		for num := currNumber; num > targetNumber; num-- {
			migration, ok := conn.migrations[num]
			if !ok {
				break
			}
			log.Debug().Msgf("reverse migration %d", num)

			if migration.Reverse != nil {
				if err = migration.Reverse(conn); err != nil {
					return err
				}
			}
			if err = conn.DeleteOne(ctx, conn.migrationsCol, M{"number": num}); err != nil {
				return err
			}
		}
	} else {
		for num := currNumber + 1; num != targetNumber; num++ {
			migration, ok := conn.migrations[num]
			if !ok {
				break
			}
			log.Debug().Msgf("forward migration %d", num)

			if migration.Forward != nil {
				if err = migration.Forward(conn); err != nil {
					return err
				}
			}

			doc := migrationDoc{
				Created: time.Now(),
				Number:  num,
			}
			if err = conn.InsertOne(ctx, conn.migrationsCol, doc, &doc.ID); err != nil {
				return err
			}
		}
	}

	log.Debug().Msg("migrations done")

	return nil
}
