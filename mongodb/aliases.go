package mongodb

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
)

const (
	EQ     = "$eq"
	NE     = "$ne"
	GT     = "$gt"
	GE     = "$gte"
	LT     = "$lt"
	LE     = "$lte"
	REGEX  = "$regex"
	EXISTS = "$exists"
	IN     = "$in"
	OR     = "$or"
	AND    = "$and"
	SET    = "$set"
)

type (
	M = bson.M
	A = bson.A

	PK = string // string primary key

	Context = context.Context
)
