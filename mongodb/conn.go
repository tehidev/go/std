package mongodb

import (
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/tehidev/go/monolog"
)

type Conn struct {
	db *mongo.Database

	logger monolog.Logger

	migrations    map[int]Migration
	migrationsCol string
}

func (conn *Conn) Database() *mongo.Database {
	return conn.db
}

func (conn *Conn) Close(ctx Context) error {
	_, log := conn.logger.Branch(ctx, "mongodb")
	defer log.End()

	log.Debug().Msg("connection closing")
	if err := conn.db.Client().Disconnect(ctx); err != nil {
		err = errors.WithMessage(err, "client.Disconnect")
		log.Warn().Err(err).Msg("connection closed")
		return err
	}
	log.Debug().Msg("connection closed")
	return nil
}
